<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ include file="/WEB-INF/include/common.jsp" %>
		<c:set var="pageName">トップページ</c:set>
		<!DOCTYPE html>
		<html>

		<head>
			<meta charset="UTF-8">
			<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
			<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
			<title>${pageName}:${siteName}</title>
			<link rel="stylesheet" href="css/index.css">
		</head>

		<body>
			<header class="container-fruid">
				<%@ include file="/WEB-INF/include/header.jsp" %>
			</header>
			<!-- ジャンボトロン -->
			<div class="jumbotron jumbotron-fruid">
				<div>
					<h1 class="display-4">${siteName}</h1>

				</div>
			</div>

			<article class="container my-4 py-4">
				<section class="row">
					<h1 class="col-12 border-bottom my-4">${pageName}</h1>
					<!-- コンテンツ１ -->
					<div class="col-4 text-center">
						<img src="img/011.jpg" alt="" class="rounded-circle" width="140" height="140" />
						<h2 class="h2">会社情報</h2>
						<p></p>
						<a href="#contents1" class="btn btn-primary">会社情報へ</a>
					</div>
					<!-- コンテンツ２ -->
					<div class="col-4 text-center">
						<img src="img/012.jpg" alt="" class="rounded-circle" width="140" height="140" />
						<h2 class="h2">製品情報</h2>
						<a href="#contents2" class="btn btn-primary">製品情報へ</a>
					</div>
					<!-- コンテンツ３ -->
					<div class="col-4 text-center">
						<img src="img/013.jpg" alt="" class="rounded-circle" width="140" height="140" />
						<h2 class="h2">採用情報</h2>
						<a href="#contents3" class="btn btn-primary">採用情報へ</a>
					</div>
				</section>
				<!-- コンテンツ１ -->
				<div class="row my-5">
					<section class="col-12">
						<h2 class="h2" id="contents1">会社情報</h2>
						<div class="bg-info">
							<p>弊社では、ーー分野の製品プロデュース業および、</p>
							<p>＊＊分野での総合ソリューション提供事業を行っています。</p>
							<p>年間XX０案件の受発注実績あり。</p>
							<p>年間売上実績　約００XX０円（※００XX年のデータに基づく）</p>

						</div>
						<a href="${sitePath}/about" class="nav-link">会社概要へ</a>
					</section>
				</div>
				<!-- コンテンンツ２ -->
				<div class="row my-5">
					<section class="col-12">
						<h2 class="h2" id="contents2">製品情報</h2>
						<p>自社開発製品「hogehugaX」</p>
						<img src="img/001.jpg" alt="" class="img-fluid">
						<p>
							既存の従来型システムを凌駕した、圧倒的ハイスペック機能搭載。
							<br> ーー分野での、問題解決に最適な機能をこの一台に搭載。
							<br> コストパフォーマンス面でも、弊社従来型と比較して約１０％のコストカットに成功。
							<br> 不要な工程を削減することで弊社特別価格による提供を実現。
						</p>

						<a href="${sitePath}/member" class="nav-link">製品情報詳細へ</a>
					</section>
				</div>
				<!-- コンテンンツ３ -->
				<div class="row my-5">
					<section class="col-12">
						<h2 class="h2" id="contents3">採用情報</h2>
						<div class="image-back-con3">
							<h3 class="h3" id="contents3">新卒採用</h3>
							<p>20XX年度　随時募集中</p>
							<p>まずは、お気軽にお問い合わせください。。</p>
							<h3 class="h3" id="contents3">中途採用</h3>
							<p>経歴不問・業界未経験者歓迎！</p>
							<p>職務経歴書・及び履歴書ご用意の上、お問い合わせください。</p>
						</div>
							<p>各採用の詳細は以下のリンクへ</p>
							<a href="${sitePath}/gallery" class="nav-link">採用情報へ</a>
					</section>
					</div>
			</article>
			<footer class="container--fruid py-4 bg-light">
				<%@ include file="/WEB-INF/include/footer.jsp" %>
			</footer>
		</body>

		</html>