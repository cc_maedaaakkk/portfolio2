<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/include/common.jsp" %>
     <c:set var="pageName">会社概要</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<title>${pageName}:${siteName}</title>
</head>
<body>
<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
</header>

<article class="container my-4 py-4">
	<section class="row">
		<h1 class="col-12 border-bottom my-4">${pageName}</h1>
		<ul>
				<li>００XX年 ０月　（株）サンプルコーポレーション前身P.S.C　設立<br>
				創設者　XXXX　XXX氏他、計７名のベンチャー企業として、<br>
				主にーー分野の総合請負会社として発足。
				</li>
				<li>０００X年 X月～　ーー分野のみならず＊＊分野の事業についても請負業を開始。<br>
				さらに自社開発にも着手し、ーー分野において初の完全自社製品「hogehugaX」を開発。<br>
				「hogehugaX」の売れ行き好調に基づき、


				</li>
				<li>０XX０年 X月　一部上場企業として株式資本へと移行。<br>
				この時より社名を、（株）サンプルコーポレーションへと変更。<br>
				総従業員数　０XX人を突破。
				</li>
				<li></li>
				<li></li>
			</ul>
	</section>
</article>

<footer class="container-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>
</body>
</html>