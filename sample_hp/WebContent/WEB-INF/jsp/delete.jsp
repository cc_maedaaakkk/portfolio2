<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ include file="/WEB-INF/include/common.jsp" %>
	<c:set var="pageName">削除</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${pageName}</title>
</head>
<body>
<h1>記事を削除しました。</h1>

	<div>
		<p class="text-right">
			<a href="${sitePath}/blog">ブログ一覧へ</a>
		</p>
	</div>

</body>
</html>