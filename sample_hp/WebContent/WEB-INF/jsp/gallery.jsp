<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">採用情報</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<title>${pageName}:${siteName}</title>
</head>
<body>
<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
</header>
<article class="container my-4 py-4">
	<section class="row">
		<h1 class="col-12 border-bottom my-4">${pageName}</h1>
		<c:forEach var="gallery" items="${galleryList}">
			<div class="col-3">
				<a href="${sitePath}/img/${gallery.src}">
					<img src="${sitePath}/img/${gallery.src}" alt="${gallery.name}" class="img-thumbnail" />
				</a>
				<h3>${gallery.name}</h3>
				<p>${gallery.comment}</p>
			</div>
		</c:forEach>
	</section>
	
	
</article>
<footer class="container-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>
</body>
</html>