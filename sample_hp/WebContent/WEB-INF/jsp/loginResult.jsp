<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.User" %>
    <%
    User loginUser = (User) session.getAttribute("loginUser");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理者確認画面</title>
</head>
<body>
	<%
		if(loginUser != null){
	%>
	<h1>認証されました</h1>
	<a href="/sample_hp/blog_create">News作成へ</a>
	<%
		} else{
	%>
	<p>パスワードが間違っています。</p>
	<a href="/sample_hp/login_page">再入力へ</a>
	<%
		}
	%>
</body>
</html>