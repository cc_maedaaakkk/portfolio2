<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">${member.name}のページ</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<title>${pageName}:${siteName}</title>
</head>
<body>
<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
</header>
<article class="container my-4 py-4">
	<section class="row">
		<h1 class="col-12 border-bottom my-4">${member.name}のページ</h1>
		<div class="media col-12">
		<img class="mr-3" src="${sitePath}/img/${member.imgSrc}" alt="${member.name}の画像" style="max-width: 320px;">
			<div class="media-body">
				<h5 class="mt-0">${member.name}</h5>
				<p>参考価格：${member.age}</p>
				<p>${member.comment}</p>
			</div>
		</div>
	</section>
	<div>
		<p class="text-right">
			<a href="${sitePath}/member" class="btn btn-link">製品一覧に戻る</a>
		</p>
	</div>

</article>

<footer class="container-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>
</body>
</html>