<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">製品一覧</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<title>${pageName}:${siteName}</title>
</head>
<body>
<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
</header>
<article class="container my-4 py-4">
	<section class="row">
		<h1 class="col-12 border-bottom my-4">${pageName}</h1>
		<c:forEach var="member" items="${memberList}">
			<div class="card col-3">
				<img src="${sitePath}/img/${member.imgSrc}" alt="${member.name}の画像" class="card-img-top" style="max-width: 300px;" />
				<div class="card-body">
					<h3 class="card-title">${member.name}</h3>
					<p class="card-text">参考価格：${member.age}</p>
				</div>
				<div class="card-footer">
					<a href="${sitePath}/member?id=${member.id}" class="btn btn-primary btn-block">about</a></div>
			</div>
		</c:forEach>
	</section>
</article>
<footer class="container--fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>
</body>
</html>
