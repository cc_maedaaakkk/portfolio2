<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">記事が見つかりません</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<c:choose>
	<c:when test="${blog==null}">
		<title>${pageName}:${siteName}</title>
	</c:when>
	<c:otherwise>
		<title>${blog.title}:${siteName}</title>
	</c:otherwise>
</c:choose>

</head>
<body>
<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
</header>
<article class="container my-4 py-4">
	<section class="row">
		<c:choose>
			<c:when test="${blog==null}">
				<h1 class="col-12 border-bottom my-4">${pageName}</h1>
			</c:when>
			<c:otherwise>
				<div class="col-12">
					<time>${blog.createAt}</time>
				</div>
				<h1 class="col-12 border-bottom my-4">${blog.title}</h1>
				<p>${blog.article}</p>
			</c:otherwise>
		</c:choose>
	</section>

	<div>
		<p class="text-right">
			<a href="${sitePath}/blog_delete?id=${blog.id}">ブログ削除</a>
		</p>
	</div>

	<div>
		<p class="text-right">
		<a href="${sitePath}/blog">ブログ一覧へ</a>
		</p>
	</div>

</article>
<footer class="container--fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>
</body>
</html>