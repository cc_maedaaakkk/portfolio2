<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav class="nav bar-expand-lg fixed-top navbar-light bg-light">
	<a class="navbar-brand" href="/">${siteName}</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarNav" aria-controls="navbarNav"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item"><a href="${sitePath}/" class="nav-link">Home</a></li>
			<li class="nav-item"><a href="${sitePath}/about" class="nav-link">About</a></li>
			<li class="nav-item"><a href="${sitePath}/member" class="nav-link">Products</a></li>
			<li class="nav-item"><a href="${sitePath}/gallery" class="nav-link">Adopt</a></li>
			<li class="nav-item"><a href="${sitePath}/blog" class="nav-link">News</a></li>
		</ul>
	</div>
</nav>