<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<ul class="nav mx-auto w-50">
	<li class="nav-item"><a href="${sitePath}/" class="nav-link">Home</a></li>
	<li class="nav-item"><a href="${sitePath}/about" class="nav-link">About</a></li>
	<li class="nav-item"><a href="${sitePath}/member" class="nav-link">Products</a></li>
	<li class="nav-item"><a href="${sitePath}/gallery" class="nav-link">Adopt</a></li>
	<li class="nav-item"><a href="${sitePath}/blog" class="nav-link">News</a></li>
</ul>
<div class="text-center"><small>&copy;${siteName} all right reserved</small></div>