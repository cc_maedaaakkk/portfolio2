package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Blog;

public class BlogDAO {

	private final String DRIVER_NAME = "com.mysql.jdbc.Driver";

//	private final String DB_URL = "jdbc:mysql://localhost:3306/";
//	private final String DB_NAME = "portfolio1";
//	private final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";
//	private final String DB_USER = "root";
//	private final String DB_PASS = "root";

		private final String DB_URL = "jdbc:mysql://jspcloud10.raservers.net/";
		private final String DB_NAME = "koukimae_portfolio2";
		private final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";
		private final String DB_USER = "koukimae_da";
		private final String DB_PASS = "ioYkspzbHQMr6sx1";

	public List<Blog> findAll() {
		Connection conn = null;
		List<Blog> blogList = new ArrayList<>();

		try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

			String sql = "SELECT id,create_at,title,article FROM blog ORDER BY create_at DESC ";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String createAt = rs.getString("create_at");

				String title = rs.getString("title");
				String article = rs.getString("article");

				Blog blog = new Blog(createAt, title, article);
				blog.setId(id);
				blogList.add(blog);
			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return blogList;
	}

	public Blog find(int index) {
		Connection conn = null;
		Blog blog = null;

		try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);
			String sql = "SELECT id,create_at,title,article FROM blog WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, index);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String createAt = rs.getString("title");
				String title = rs.getString("title");
				String article = rs.getString("article");
				blog = new Blog(createAt, title, article);

				blog.setId(id);
			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return blog;

	}

	public boolean create(Blog blog) {
		Connection conn = null;
		try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);
			String sql = "INSERT INTO blog(create_at,title,article) VALUES(?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, blog.getCreateAt());
			pStmt.setString(2, blog.getTitle());
			pStmt.setString(3, blog.getArticle());

			int result = pStmt.executeUpdate();
			if (result != 1) {
				return false;
			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public void delete(int id) {
		Connection conn = null;

		try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);
			String sql = "DELETE FROM blog WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			pStmt.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

}
