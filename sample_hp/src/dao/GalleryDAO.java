package dao;

import java.util.ArrayList;
import java.util.List;

import model.Gallery;

public class GalleryDAO {
	public List<Gallery> findAll(){
		List<Gallery> galleryList = new ArrayList<>();
		galleryList.add(new Gallery(1,"総合職（総務担当）","005.jpg","なんたらかんたら"));
		galleryList.add(new Gallery(2,"総合職（営業担当）","006.jpg","なんたらかんたら"));
		galleryList.add(new Gallery(3,"一般職（事務）","007.jpg","なんたらかんたら"));
		galleryList.add(new Gallery(4,"一般職（経理）","008.jpg","なんたらかんたら"));
		galleryList.add(new Gallery(5,"契約社員（地域固定型）","009.jpg","なんたらかんたら"));
		galleryList.add(new Gallery(6,"その他","010.jpg","なんたらかんたら"));
		return galleryList;

	}

}
