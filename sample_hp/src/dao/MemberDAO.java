package dao;

import java.util.ArrayList;
import java.util.List;

import model.Member;

public class MemberDAO {
	public List<Member> findAll(){
		List<Member> memberList = new ArrayList<>();
		memberList.add(new Member(1,"hogehugaX",230000,"001.jpg","製品詳細：、、、、。"));
		memberList.add(new Member(2,"hogehugaWX",220000,"002.jpg","製品詳細：、、、、、。"));
		memberList.add(new Member(3,"hogehugaXX-I",320000,"003.jpg","製品詳細：、、、、、。"));
		memberList.add(new Member(4,"hogehugaZ-X",250000,"004.jpg","製品詳細、、、、、。"));
		return memberList;
	}

	public Member find(int index) {
		Member member = findAll().get(index-1);
		return member;

	}

}
