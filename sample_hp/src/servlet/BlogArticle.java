package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Blog;
import model.GetBlogArticleLogic2;

/**
 * Servlet implementation class BlogArticle
 */
@WebServlet("/article")
public class BlogArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forwardPath;
		GetBlogArticleLogic2 getBlogArticleLogic = new GetBlogArticleLogic2();
		String idString = request.getParameter("id");
		if(idString == null) {
			request.setAttribute("blog", null);
			forwardPath = "WEB-INF/jsp/article.jsp";
		} else {
			int id = Integer.parseInt(idString);
			Blog blog = getBlogArticleLogic.execute(id);
			request.setAttribute("blog", blog);
			forwardPath = "WEB-INF/jsp/article.jsp?id=" + id;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}

}
