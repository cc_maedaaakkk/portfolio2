package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Blog;
import model.PostBlogLogic;

/**
 * Servlet implementation class BlogCreate
 */
@WebServlet("/blog_create")
public class BlogCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = sdf.format(date);
		request.setAttribute("now", dateString);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/blog_create.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Blog blog = null;
		String createAt = request.getParameter("create_at");
		String title = request.getParameter("title");
		String article = request.getParameter("article");

		blog = new Blog(createAt,title,article);

		PostBlogLogic postBlogLogic = new PostBlogLogic();
		postBlogLogic.execute(blog);
		response.sendRedirect("blog");
	}

}
