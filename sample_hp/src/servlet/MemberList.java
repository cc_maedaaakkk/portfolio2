package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GetMemberListLogic;
import model.GetMemberLogic;
import model.Member;

/**
 * Servlet implementation class MemberList
 */
@WebServlet("/member")
public class MemberList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher;
		String forwardPath = "";
		String id = request.getParameter("id");

		if(id == null || id.length() == 0) {
			GetMemberListLogic getMemberListLogic = new GetMemberListLogic();
			List<Member> memberList = getMemberListLogic.execute();
			request.setAttribute("memberList", memberList);
			forwardPath = "/WEB-INF/jsp/member_list.jsp";
		}else {
			int index = Integer.parseInt(id);
			GetMemberLogic getMemberLogic = new GetMemberLogic();
			Member member = getMemberLogic.execute(index);
			request.setAttribute("member", member);
			forwardPath = "/WEB-INF/jsp/member.jsp?id=" +index;
		}
		dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}

}
