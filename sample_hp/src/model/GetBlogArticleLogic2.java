package model;

import dao.BlogDAO;

public class GetBlogArticleLogic2 {
	public Blog execute(int id) {
		BlogDAO dao = new BlogDAO();
		return dao.find(id);
	}
}
