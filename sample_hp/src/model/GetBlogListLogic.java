package model;

import java.util.List;

import dao.BlogDAO;

public class GetBlogListLogic {
	public List<Blog> execute(){
		BlogDAO dao = new BlogDAO();
		return dao.findAll();
	}
}
