package model;

import java.io.Serializable;

public class Blog implements Serializable {
	private int id;
	private String createAt;
	private String title;
	private String artcle;

	public Blog() {}

	public Blog( String createAt, String title, String article) {

		this.createAt = createAt;
		this.title = title;
		this.artcle = article;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArticle() {
		return artcle;
	}

	public void setArticle(String artcle) {
		this.artcle = artcle;
	}



}
