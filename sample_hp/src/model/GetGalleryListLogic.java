package model;

import java.util.List;

import dao.GalleryDAO;

public class GetGalleryListLogic {
	public List<Gallery> execute(){
		GalleryDAO dao = new GalleryDAO();
		return dao.findAll();

	}
}
