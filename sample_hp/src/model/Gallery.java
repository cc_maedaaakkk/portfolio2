package model;

import java.io.Serializable;

public class Gallery implements Serializable {
	private int id;
	private String name;
	private String src;
	private String comment;

	public Gallery() {}

	public Gallery(int id, String name, String src, String comment) {
		super();
		this.id = id;
		this.name = name;
		this.src = src;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}


}
