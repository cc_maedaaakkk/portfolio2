package model;

import java.io.Serializable;

public class User implements Serializable {
	private String pass;

	public User() {}
	public User(String pass) {
		this.pass = pass;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}



}
